import io.tecky.java_demo.utils.JSArray;
import zoo.Animal;


//import java.util.Arrays;
//import java.util.Collections;
//import java.util.List;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world");

//        Animal cat = new Animal();
        Animal cat = new zoo.Cat();
//        zoo.Cat cat = (zoo.Cat) new Animal();
        cat.move();

        int[] numbers = new int[3];
        numbers[0] = 1;
        numbers[1] = 2;
        numbers[2] = 3;

//        String text = Stream.of(1, 2, 3).map(String::valueOf).collect(Collectors.joining(", "));

        String text = JSArray.join(numbers);

        System.out.println(text);

    }
}
