package io.tecky.java_demo.utils;

public class JSArray {
    public static String join(int[] numbers, String separator) {
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < numbers.length; i++) {
            if (i == 0) {
                text.append(i);
            } else {
                text.append(separator).append(i);
            }
        }
        return text.toString();
    }
    public static String join(int[] numbers) {
       return join(numbers,",");
    }
}
