package io.tecky.java_demo.utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

enum GameStatus {
    GAME_OVER,
    PLAYING
}

public class GuessNumberGame {
    int lower = 1;
    int upper = 100;
    int answer;
    Scanner scanner;

    GameStatus status = GameStatus.PLAYING;

    public GuessNumberGame(Scanner scanner) {
        this.scanner = scanner;
        Random random = new Random();
        int range = upper - lower + 1;
        answer = Math.abs(random.nextInt()) % range + lower;
    }

    void guessOnce() {
        System.out.print("Guess a number, " + lower + " - " + upper + " : ");
        int guess = scanner.nextInt();
        if (guess == answer) {
            System.out.println("Correct");
            this.status = GameStatus.GAME_OVER;
            return;
        }
        if (guess < answer) {
            System.out.println("Too small");
            lower = guess;
            return;
        }
        // guess > answer
        System.out.println("Too big");
        upper = guess;
    }

    void start() {
        System.out.println("answer: " + answer);
        while (status == GameStatus.PLAYING) {
            guessOnce();
        }
    }

    public static void main(String[] args) {
        InputStreamReader reader = new InputStreamReader(System.in);
//        BufferedInputStream inputStream  = new BufferedInputStream(System.in);
        Scanner scanner = new Scanner(reader);
        GuessNumberGame game = new GuessNumberGame(scanner);

        game.start();
    }
}
